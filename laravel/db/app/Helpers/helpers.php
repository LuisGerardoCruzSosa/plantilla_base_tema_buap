<?php

if (!function_exists('image_url')) {
    function image_url($path)
    {
        return asset('db/' . $path);
    }
}

if (!function_exists('page_url')) {
    function page_url($path)
    {
        return url('db/' . $path);
    }
}
