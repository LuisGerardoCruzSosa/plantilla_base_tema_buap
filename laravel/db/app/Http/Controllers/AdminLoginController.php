<?php
 
 namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AdminLoginController extends Controller
{
    public function showLoginForm()
    {
        if (Auth::check()) {

            return redirect()->route('report');
        }
        
        return view('admin/acceso'); 
    }


    public function loginadmin(Request $request)
    {
        $user = User::where('username', $request->only('username'))->first();
        if ( $user )  {
            $credentials = array('username' => $request->input('username'), 'password' => md5($request->input('password')));
            if ( $user->getAttributeValue('password') == md5($request->input('password')) ) {
                Auth::login($user);
                session()->put('login', true);
                
                return redirect()->route('report');
            } else {
               
                return back()->with('error', 'Datos incorrectos.');
            }

        } else {
            return back()->with('error', 'Datos incorrectos.');
        }
    }

    public function logoutadmin()
    {
        session()->forget('login');
        Auth::logout(); 
        return redirect()->route('adminlogin');
    }

}
