<?php

namespace App\Http\Controllers;

use App\Models\Lectura;
use Illuminate\Http\Request;

class LecturaController extends Controller
{

    public function procesaLectura(Request $request)
    {
        $randomPage = rand(0, 1) ? route('text04Lectura1') : route('text04Lectura2');
        
        return redirect($randomPage);
    }
}
