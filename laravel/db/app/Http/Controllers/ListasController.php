<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exam;

class ListasController extends Controller
{
    public function index()
    {
        $exams = Exam::all();
        return view('user.listas', compact('exams')); 
    }
}