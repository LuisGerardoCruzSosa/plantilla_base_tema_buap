<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\register; 

class RegistroController extends Controller
{
    public function usuario(Request $request, $idExamen)
    {

        $validated = $request->validate([
            'apellidos' => 'required|string|max:100',
            'nombres' => 'required|string|max:100',
            'correo' => 'required|string|email|max:100',
            'fecha' => 'required|date',
            'genero' => 'required|in:0,1',
            'egresado' => 'required|in:0,1',
            'matricula' => 'nullable|digits:9'
        ]);

        register::create([
            'lastname' => $validated['apellidos'],
            'name' => $validated['nombres'],
            'mail' => $validated['correo'],
            'birthday' => $validated['fecha'],
            'graduate' => $validated['egresado'],
            'matricula' => $validated['matricula'] ?? null,
            'gender_id' => $validated['genero'],
            'date_register' => now(),
            'exams_id' => $idExamen,
        ]);

        return redirect()->route('section', ['section_id' => 1])->with('success', 'Registro exitoso'); 

        
    }
}

