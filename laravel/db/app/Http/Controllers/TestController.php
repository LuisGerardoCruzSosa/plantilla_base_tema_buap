<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\section;

class TestController extends Controller
{
    public function showSection()
    {
        $test = Test::find(1); 
        return view('user.test02', ['description' => $test->description_section]);
    }
}