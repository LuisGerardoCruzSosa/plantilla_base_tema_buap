<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'answers';
    protected $primaryKey = 'id';
    protected $fillable = ['answer_text', 'questions_id'];
 
    public function question()
    {
        return $this->belongsTo(question::class, 'questions_id');
    }
}
    


