<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Encuesta extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'encuesta';
    protected $primaryKey = 'id';
    protected $fillable = ['created_at', 'user_id', 'register_id', 'register_user_id', 'register_exams_id', 'register_gender_id', 'section_id', 'section_exams_id'];
 						

    public function section(){
        return $this->belongTo(section::class, 'section_id', 'section_exams_id');
    }
    public function register()
    {
        return $this->belongsTo(register::class, ['register_id', 'register_exams_id', 'register_gender_id']);
    }
}
