<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;
        protected $table = 'exams';
        protected $primaryKey = 'id';
        protected $fillable = ['title', 'description', 'dateStart', 'dateEnd', 'active'];

        public $timestamps = false;

        public function sections()
        {
            return $this->hasMany(Section::class, 'exams_id');
        }
    
        public function registers()
        {
            return $this->hasMany(Register::class, 'exams_id');
        }
}
