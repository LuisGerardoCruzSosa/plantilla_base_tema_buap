<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    use HasFactory;

        protected $table = 'gender';
        protected $primaryKey = 'id';
        protected $fillable = ['gender'];
        
        public $timestamps = false;
    
        public function registers()
        {
            return $this->hasMany(Register::class, 'gender_id');
        }

}

