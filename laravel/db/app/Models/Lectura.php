<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lectura extends Model
{
    use HasFactory;
    protected $table = 'lecturas';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['texto_lectura', 'tiempo', 'section_id', 'section_exams_id'];
    
    public function section()
        {
            return $this->belongsTo(Section::class, ['section_id', 'section_exams_id'], ['id', 'exams_id']);
        }
    
}
