<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;
    protected $table = 'logs';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['content', 'action', 'date'];
    
    public function filterLogs()
    {
        return $this->hasMany(FilterLog::class, 'logs_id');
    }
}



