<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;
    protected $table = 'permissions';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['name_permissions'];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'permissions_has_role', 'permissions_id', 'role_id');
    }
    
}
