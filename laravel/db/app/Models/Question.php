<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $table = 'questions';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['question_text', 'section_id', 'type_question_id'];

    // public function answers(){
    //     return $this->belongTo(section::class, 'questions_id');
    // }
 	
    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }
    
    public function typeQuestion()
    {
        return $this->belongsTo(TypeQuestion::class, 'type_question_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'questions_id');
    }
}


