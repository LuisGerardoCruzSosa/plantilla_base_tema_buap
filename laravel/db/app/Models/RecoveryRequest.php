<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecoveryRequest extends Model
{
    use HasFactory;
    protected $table = 'recovery_request';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['request_date', 'validated', 'register_id', 'register_exams_id', 'register_gender_id'];
   
    public function register()
    {
        return $this->belongsTo(Register::class, ['register_id', 'register_exams_id']);
    }
}


