<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class register extends Model
{
    use HasFactory;
    
        protected $table = 'register';
        protected $primaryKey = 'id';
        protected $fillable = ['lastname', 'name', 'mail', 'birthday', 'graduate', 'matricula', 'date_register', 'estado_acceso', 'exams_id', 'gender_id'];
        public $timestamps = false;

        public function exam()
        {
            return $this->belongsTo(Exam::class, 'exams_id');
        }
        
        public function gender()
        {
            return $this->belongsTo(Gender::class, 'gender_id');
        }
        
        public function encuestas()
        {
            return $this->hasMany(Encuesta::class, 'register_id');
        }

}



