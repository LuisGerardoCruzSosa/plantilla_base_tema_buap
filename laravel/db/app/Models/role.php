<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    use HasFactory;
    protected $table = 'role';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['role_name'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_has_user', 'role_id', 'user_id');
    }
    
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permissions_has_role', 'role_id', 'permissions_id');
    }
}



