<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class section extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'section';
    protected $primaryKey = 'id';
    protected $fillable = ['name_section', 'description_section', 'exams_id'];

    public function exam()
    {
        return $this->belongsTo(Exam::class, 'exams_id');
    }
    
    public function questions()
    {
        return $this->hasMany(Question::class, 'section_id');
    }
}
