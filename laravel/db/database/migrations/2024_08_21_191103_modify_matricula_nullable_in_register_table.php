<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyMatriculaNullableInRegisterTable extends Migration
{
    public function up()
    {
        Schema::table('register', function (Blueprint $table) {
            $table->string('matricula', 9)->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('register', function (Blueprint $table) {
            $table->string('matricula', 9)->nullable(false)->change();
        });
    }
}

