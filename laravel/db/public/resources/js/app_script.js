
(function($) {
    $.sanitize = function(input) {
        if ( input != null && typeof input === 'string')
            return $.trim(input.replace(/<script[^>]*?>.*?<\/script>/gi, '').replace(/<[\/\!]*?[^<>]*?>/gi, '').replace(/<style[^>]*?>.*?<\/style>/gi, '').replace(/<![\s\S]*?--[ \t\n\r]*>/gi, ''));
        return "";
    };
})(jQuery);

//PopoverAdministrador
$(document).ready(function(){
    $('[data-bs-toggle="popover"]').popover({
        trigger: 'hover',
        placement: 'top'
    });
});


//Pagina registro
function validar() {
    // Validación de nombres
    var nombres = document.getElementById('nombres').value;
    var nombreExpr = /^[a-zA-ZáéíóúüÁÉÍÓÚÜñÑ\'' s]+$/;
    if (nombres.length > 100) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "El campo \"Nombre(s)\" no puede exceder 100 caracteres",
        });
        return false;
    }
    if (!nombreExpr.test(nombres)) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "El campo \"Nombres(s)\" únicamente puede contener letras",
          });
        return false;
    }

    // Validación de apellidos
    var apellidos = document.getElementById('apellidos').value;
    if (apellidos.length > 100) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "El campo \"Apellido(s)\" no puede exceder 100 caracteres",
        });
        return false;
    }
    if (!nombreExpr.test(apellidos)) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "El campo \"Apellido(s)\" únicamente puede contener letras",
          });
          return false;
        }
        
        // Validación de correo electrónico
        var correo = document.getElementById('correo').value;
        var expr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (correo.length > 100) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "El campo \"Correo\" no puede exceder 100 caracteres",
            });
            return false;
        }
        if (!expr.test(correo)) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "Escriba una dirección de correo válida",
              });
              return false;
            }
            
            //Fecha de Nacimiento
            var fechaNacimiento = document.getElementById('fecha').value;
            var fechaMinima = new Date ('1950-01-01');
            var fechaMaxima = new Date();
            var fechaUsuario = new Date( fechaNacimiento);
            if ( fechaUsuario < fechaMinima || fechaUsuario > fechaMaxima){
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Debes seleccionar una fecha correcta",
                  });
                  return false;
                }
                
                // Validación de Género
                var genero = document.getElementById('genero').value;
                if (genero === "null") {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Debes seleccionar un género",
                  });
                  return false;
                }
                
                // Validación de si es egresado
                var egresado = document.getElementById('egresado').value;
                if (egresado === "null") {
                    Swal.fire({
                        icon: "error",
                        title: "Oops...",
                        text: "Debes seleccionar una opción para egresado BUAP",
                      });
                      return false;
                    } else if (egresado === "1") {
                        var matricula = document.getElementById('matricula').value;
                        var matriculaExpr = /^\d{9}$/; // Solo dígitos y exactamente 9 caracteres
                        if (!matriculaExpr.test(matricula)) {
                            Swal.fire({
                                icon: "error",
                                title: "Oops...",
                                text: "Por favor ingrese su matrícula BUAP a nueve dígitos, solo números",
                              });
            return false;
        }
    }
    
    return true;
}

//Funcion de egresado
function toggleMatricula() {
    var egresado = document.getElementById('egresado').value;
    var matriculaDiv = document.getElementById('matriculaDiv');
    if (egresado === "1") {
        matriculaDiv.style.display = 'block';
    } else {
        matriculaDiv.style.display = 'none';
    }
}

/*
//Validación de lectura 1
function validarFormulario4_L1() {
    var preguntas = [
        "question-01",
        "question-02",
        "question-03",
        "question-04",
        "question-05"
    ];

    for (var i = 0; i < preguntas.length; i++) {
        var opciones = document.getElementsByName(preguntas[i]);
        var seleccionado = false;
        
        for (var j = 0; j < opciones.length; j++) {
            if (opciones[j].checked) {
                seleccionado = true;
                break;
            }
        }

        if (!seleccionado) {         
             Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Por favor, responde a la pregunta " + (i + 1),
          });
            return false;
        }
    }
    return true;
}

//Validación de lectura 2
function validarFormulario4_L2() {
    var preguntas = [
        "question-01",
        "question-02",
        "question-03",
        "question-04",
        "question-05"
    ];

    for (var i = 0; i < preguntas.length; i++) {
        var opciones = document.getElementsByName(preguntas[i]);
        var seleccionado = false;
        
        for (var j = 0; j < opciones.length; j++) {
            if (opciones[j].checked) {
                seleccionado = true;
                break;
            }
        }

        if (!seleccionado) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "Por favor, responde a la pregunta " + (i + 1),
              });
            return false;
        }
    }
    return true;
}

*/

//Validación de la pagina encuesta
function validarEncuesta() {
    var preguntas = {
        "1": "Nivel de satisfacción con la plataforma",
        "2": "Diseño",
        "3": "Facilidad de uso",
        "4": "Rapidez de descarga de las páginas",
        "5": "Tipo de preguntas",
        "6": "Soporte técnico"
    };

    for (var i = 1; i <= 6; i++) {
        var questionNum = i === 1 ? '01' : '0' + i;
        if (!document.querySelector('input[name="question-' + questionNum + '"]:checked')) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "Por favor, responde a la pregunta  " + "<strong>" + preguntas[i] + "</strong>",
            });
            return false;
        }
    }

    const textarea = document.querySelector('textarea[name="question-07"]');
    const value = textarea.value.trim();
    if (textarea.value.trim() === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            html: "Por favor, responde a la pregunta <strong> sugerencias y/o comentarios </strong>",
        });
        return false;
    }

        if (value.length > 500) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "La respuesta a la pregunta <strong>sugerencias y/o comentarios</strong> no puede exceder los 500 caracteres.",
            });
            return false;
        }

        if (/[^a-zA-Z0-9\s.,-_¿?¡!'"“”()áéíóúÁÉÍÓÚñÑ]/.test(value)) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "La respuesta a la pregunta <strong>sugerencias y/o comentarios</strong> no puede contener caracteres especiales.",
            });
            return false;
        }
    
    return true;
}

//Confirmacion de Delete en la pagina Admisnitrador
function confirmDelete() {
    Swal.fire({
        title: "¿Estás seguro de eliminar?",
        text: "Esta acción no se podrá revertir.",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#3085d6",
        confirmButtonText: "Sí, eliminar",
        cancelButtonText: "Cancelar"
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire({
                title: "Eliminado",
                icon: "success",
                confirmButtonText: "Aceptar"
            });
        }
    });
}

//Lista de opciones en Tipo de respuesta
function mostrarDiv() {
    const tipoRespuesta = document.getElementById("tipoRespuesta").value;
    document.getElementById("opcionesMultiplesDiv").style.display = tipoRespuesta === "opcionesMultiples" ? "block" : "none";
    document.getElementById("parrafoDiv").style.display = tipoRespuesta === "parrafo" ? "block" : "none";
}

//Agrega nueva opcion en Respuesta de Opciones Multiples
function agregarOpcion() {
    const divOpciones = document.getElementById("opcionesMultiplesDiv");
    const nuevaOpcion = document.createElement("div");
    nuevaOpcion.classList.add("form-check");
    nuevaOpcion.innerHTML = `
        <input type="radio" class="form-check-input" name="opcion" value="Nueva opción">
        <input required type="text" class="form-control" value="Nueva opción"><br>
    `;
    const botonAgregar = divOpciones.querySelector("button");
    divOpciones.insertBefore(nuevaOpcion, botonAgregar);
}

function validarExamen() {
    // Validación de titulo
    var titulo = document.getElementById('titulo').value;
    var tituloExpr = /^[a-zA-ZáéíóúüÁÉÍÓÚÜñÑ\'' s.]+$/;
    if (titulo.length > 500) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            html: "El campo <strong>\Titulo\</strong> no puede exceder 500 caracteres",
        });
        return false;
    }
    if (!tituloExpr.test(titulo)) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            html: "El campo <strong>\Titulo\</strong> únicamente puede contener letras",
          });
        return false;
    }

    //Validación de descripción
    const textarea = document.querySelector('textarea');
    const value = textarea.value.trim();

        if (value.length > 1000) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "La descripción no puede exceder los 1000 caracteres.",
            });
            return false;
        }
        
        if (/[^a-zA-Z0-9\s.,-_¿?¡!'"“”()áéíóúÁÉÍÓÚñÑ]/.test(value)) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "La descripción del <strong>examen</strong> no puede contener caracteres especiales.",
            });
            return false;
        }

        const fechaInicio = document.getElementById('fechaInicio').value;
        const fechaFin = document.getElementById('fechaFin').value;
        const inicio = new Date(fechaInicio);
        const fin = new Date(fechaFin);

        if (inicio.getTime() === fin.getTime()) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "La fecha de inicio <strong>no puede ser igual</strong> a la fecha de fin.",
            });
            
            return false;
        }
      
        if (fin.getTime() < inicio.getTime()) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "La fecha de fin <strong>no puede ser anterior</strong> a la fecha de inicio.",
            });
            return false;
        }

        return true;
    }

  



    //Validacion de cuestionario
    function validarCuestionario() {
        // Validación de título
        var titulo = document.getElementById('titulo').value.trim();
        var tituloExpr = /^[a-zA-ZáéíóúüÁÉÍÓÚÜñÑ\'\s.]+$/;
        if (titulo.length > 500) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "El campo <strong>Título</strong> no puede exceder 500 caracteres",
            });
            return false;
        }
        if (!tituloExpr.test(titulo)) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "El campo <strong>Título</strong> únicamente puede contener letras",
            });
            return false;
        }
    
        // Validación de descripción
        const textarea = document.getElementById('descripcion');
        const descripcion = textarea.value.trim();
        if (descripcion.length > 1000) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "La descripción no puede exceder los 1000 caracteres.",
            });
            return false;
        }

        return true;
    }

    function validarCuestionarioPRegunta() {
        // Validación de pregunta
        var pregunta = document.getElementById('pregunta').value.trim();
        console.log("Pregunta:", pregunta); // Mensaje de depuración
        if (pregunta.length > 500) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                html: "El campo <strong>Pregunta</strong> no puede exceder 500 caracteres",
            });
            return false;
        }
        return true;
    }
//Activar color de activo button_resp
document.addEventListener('DOMContentLoaded', function() {
    var buttons = document.querySelectorAll('.button_resp input[type="radio"]');

    buttons.forEach(function(button) {
        button.addEventListener('change', function() {
            var name = button.name;
            var buttonGroup = document.querySelectorAll('input[name="' + name + '"]');

            buttonGroup.forEach(function(btn) {
                btn.parentElement.classList.remove('active');
            });

            if (button.checked) {
                button.parentElement.classList.add('active');
            }
        });
    });
});

//Activar color de activo button_resp2
document.addEventListener('DOMContentLoaded', function() {
    var buttons = document.querySelectorAll('.button_resp2 input[type="radio"]');

    buttons.forEach(function(button) {
        button.addEventListener('change', function() {
            var name = button.name;
            var buttonGroup = document.querySelectorAll('input[name="' + name + '"]');

            buttonGroup.forEach(function(btn) {
                btn.parentElement.classList.remove('active');
            });

            if (button.checked) {
                button.parentElement.classList.add('active');
            }
        });
    });
});

//Activar color de activo button_resp3
document.addEventListener('DOMContentLoaded', function() {
    var buttons = document.querySelectorAll('.button_resp3 input[type="radio"]');

    buttons.forEach(function(button) {
        button.addEventListener('change', function() {
            var name = button.name;
            var buttonGroup = document.querySelectorAll('input[name="' + name + '"]');

            buttonGroup.forEach(function(btn) {
                btn.parentElement.classList.remove('active');
            });

            if (button.checked) {
                button.parentElement.classList.add('active');
            }
        });
    });
    
});