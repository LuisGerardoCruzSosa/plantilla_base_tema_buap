<!DOCTYPE html>
<html lang="es">

@include('templates.headerPage')

<body>
    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">
                                Dirección de Acompañamiento Universitario
                            </h4>
                        </div>
                    </div>
                </div>
                <header class="sb-page-header">
                    <div class="container">
                        <h1>Iniciar sesión</h1>
                    </div>
                </header>
                    
                <div class="container">
                    <div class="row">
                        <div class="wrapper">
                            <br>
                            <form class="form-group mx-sm-3 mb-2" method="post" action="{{ route('adminlogin.post') }}">
                                @csrf
                                <div class="text-center">
                                    <span class="fa-stack fa-5x">
                                        <i class="fa fa-circle fa-stack-2x text-mutedes"></i>
                                        <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                                    </span>
                                    <br><br>
                                    <div>
                                        <input type="text" class="form-control" id="username" name="username" placeholder="Nombre de usuario" required autofocus />
                                        <br/>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" required />
                                        <br/>
                                        <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar Sesión</button>  
                                        <br/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @include('templates.footer') 

    @if(session('error'))
        <script>
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "{{ session('error') }}",
            });
        </script>
    @endif
    
</body>
</html>
