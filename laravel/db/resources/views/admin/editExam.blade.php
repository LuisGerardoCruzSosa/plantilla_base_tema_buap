<!DOCTYPE html>
<html lang="es">
    
@include('templates.headerPage')

<body>

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>

                <header class="sb-page-header">
                            <div class="container">
                                <h1>Actualización de examen</h1>
                            </div>
                        </header>
<br><br>
<!-- container -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-20 col-md-15 col-lg-10">
            <!-- /.row -->

            <!-- Projects Row -->

            <form class="form-horizontal" role="form" id="form1" name="form1" method="POST" onSubmit="return validarExamen()" action="{{  route('prueba') }}">
                <div class="form-group">
                    <label for="titulo" class="form-floating mb-3">Titulo del Examen<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <input id="titulo" required type="text" class="form-control" name="titulo" placeholder="Titulo del Examen">
                    </div>
                      
                </div>

                <div class="form-group">
                    <label for="descripcion" class="form-floating mb-3">Descripción del examen<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3"  class="input-group">
                        <textarea id="descripcion" required type="text" class="form-control" name="descripcion" placeholder="Descripción del examen*" rows="5"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="fechaInicio" class="form-floating mb-3">Fecha de inicio<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <input type="date" class="form-control" name="fechaInicio" id="fechaInicio" required placeholder="dd/mm/aaaa" min="1900-01-01" max="2100-01-01">
                    </div>
                </div>

                <div class="form-group">
                    <label for="fechaFin" class="form-floating mb-3">Fecha de fin<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <input type="date" class="form-control" name="fechaFin" id="fechaFin" required placeholder="dd/mm/aaaa" min="1900-01-01" max="2100-01-01">
                    </div>
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success btn-md">
                    <i class="fa-regular fa-circle-check fa-lg" style="color: #ffffff;"></i> Actualizar
                    </button>
                    <a href="{{ route('examenes') }}" class="btn btn-danger btn-md" data-dismiss="modal"><i class="fa-solid fa-xmark fa-xl" style="color: #ffffff;"></i>Cancelar</a>
                </div>
                
            </form>
        </div>
    </div>
</div>

            </div>
        </div>
    </div>

    @include('templates.footer')
