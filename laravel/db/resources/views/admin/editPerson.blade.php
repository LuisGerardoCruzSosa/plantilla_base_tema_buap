<!DOCTYPE html>
<html lang="es">
    
@include('templates.headerPage')

<body>

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>

                <header class="sb-page-header">
                            <div class="container">
                                <h1>Actualización de persona</h1>
                            </div>
                        </header>
<br><br>
<!-- container -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-20 col-md-15 col-lg-10">
            <!-- /.row -->

            <!-- Projects Row -->

            <form class="form-horizontal" role="form" id="form1" name="form1" method="POST" onSubmit="return validar()" action="{{  route('prueba') }}" >
                <div class="form-group">
                    <label for="apellidos" class="form-floating mb-3">Apellido(s)<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <input id="apellidos" required type="text" class="form-control" name="apellidos" placeholder="Apellido(s)*">
                    </div>
                      
                </div>
                <div class="form-group">
                    <label for="nombres" class="form-floating mb-3">Nombre(s)<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <input id="nombres" required type="text" class="form-control" name="nombres" placeholder="Nombre(s)*">
                    </div>
                </div>
                <div class="form-group">
                    <label for="correo" class="form-floating mb-3">Correo electrónico<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <input id="correo" required type="text" class="form-control" name="correo" placeholder="Correo electrónico*:">
                    </div>
                </div>
                <div class="form-group">
                    <label for="fecha" class="form-floating mb-3">Fecha de nacimiento (formato: dd/mm/aaaa)<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <input type="date" class="form-control" name="fecha" id="fecha" required placeholder="dd/mm/aaaa" min="1900-01-01">
                    </div>
                </div>
                <div class="form-group">
                    <label for="genero" class="form-floating mb-3">Género<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <select id="genero" class="form-control" name="genero" required>
                            <option value="null" selected>-- Seleccione una opción --</option>
                            <option value="F">Femenino</option>
                            <option value="M">Masculino</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="egresado" class="form-floating mb-3">¿Eres egresada/o BUAP?<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <select id="egresado" class="form-control" name="egresado" onchange="toggleMatricula()" required>
                            <option value="null" selected>-- Seleccione una opción --</option>
                            <option value="Si">Si</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" id="matriculaDiv" style="display: none;">
                    <label for="matricula" class="form-floating mb-3">Matrícula BUAP (9 dígitos)<span class="text-danger">*</span>:</label>
                    <div class="input-group mb-3">
                        <input id="matricula" type="text" class="form-control" name="matricula" placeholder="Matrícula BUAP">
                    </div>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success">
                    <i class="fa-regular fa-circle-check fa-lg" style="color: #ffffff;"></i> Actualizar
                    </button>
                    <a href="{{ route('personas') }}" class="btn btn-danger"><i class="fa-solid fa-xmark fa-xl" style="color: #ffffff;"></i>Cancelar</a>
                </div> 
            </form>
        </div>
    </div>
</div>

            </div>
        </div>
    </div>


    @include('templates.footer')
