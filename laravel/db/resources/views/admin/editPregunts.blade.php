<!DOCTYPE html>
<html lang="es">
    
@include('templates.headerPage')

<body>

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>

                <header class="sb-page-header">
                            <div class="container">
                                <h1>Actualización de pregunta</h1>
                            </div>
                        </header>
<br><br>
<!-- container -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-20 col-md-15 col-lg-10">
  
        <form  class="form-horizontal" role="form" id="form1" name="form1" method="POST" onSubmit="return validarCuestionarioPRegunta()" action="{{  route('prueba') }}">
            <div class="form-group">
                <label for="pregunta">Escribe la pregunta:</label>
                <input id="pregunta" required type="text" class="form-control"  name="pregunta" placeholder="Escribe la pregunta">
               </div><br>


            <div class="form-group">
                <label for="tipoRespuesta">Selecciona el tipo de respuesta:</label>
                <select class="form-control" id="tipoRespuesta" onchange="mostrarDiv()" required>
                    <option value="" disabled selected>Selecciona una opción</option>
                    <option value="opcionesMultiples">Opciones múltiples</option>
                    <option value="parrafo">Párrafo (respuesta abierta)</option>
                </select>
            </div><br>

            <div class="form-group" id="opcionesMultiplesDiv" style="display: none;">
                <label>Respuesta (opciones múltiples):</label>
                <div class="form-check">
                    <input type="radio" class="form-check-input" name="opcion" value="Opción 1">
                    <input required type="text" class="form-control" value="Opción 1"><br>
                </div>
                <button type="button" class="btn btn-info" onclick="agregarOpcion()">
                    <i class="fa-solid fa-plus fa-lg"></i> Agregar otra opción
                </button>
            </div>

            <div class="form-group mb-3" id="parrafoDiv" style="display: none;">
                <label>Respuesta (párrafo):</label>
                <textarea class="form-control" disabled placeholder="Respuesta abierta..."></textarea>
            </div><br>

            <div class="text-center">
            <button type="submit" class="btn btn-success">
                <i class="fa-solid fa-floppy-disk fa-lg" style="color: #ffffff;"></i> Guardar
            </button>
            <a  href="{{ route('preguntas') }}" class="btn btn-danger"><i class="fa-solid fa-xmark fa-xl" style="color: #ffffff;"></i>Cancelar</a>
            </div>  
        </form>

        </div>
    </div>
</div>

            </div>
        </div>
    </div>

    @include('templates.footer')