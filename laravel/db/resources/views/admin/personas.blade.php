<!DOCTYPE html>
<html lang="es">
    
@include('templates.headerPage')

<body>

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>
                <header class="sb-page-header">
                    <div class="container">
                        <h1>Lista de personas</h1>
                        <p></p>
                    </div>
                </header>
                <br>
                <div class="text ">
                    <a href="{{ route('report') }}"  class="btn btn-primary rounded-pill btn-xs" data-dismiss="modal"> <i class="fa-solid fa-arrow-left fa-xl" style="color: #ffffff;"></i>  Regresar</a>
                </div>
                <br>
                 <table class="table table-bordered">
                    <thead class="table-info">
                        <tr>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Fecha de Nacimiento</th>
                            <th scope="col">Genero</th>
                            <th scope="col">Egresado</th>
                            <th scope="col">Matricula</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Villavicencio Rodriguez</td>
                            <td>Maria de los Angeles Veronica</td>
                            <td>alexrpnarelle@gmail.com</td>
                            <td>02/02/1999</td>
                            <td>Masculino</td>
                            <td>Si</td>
                            <td>202998543</td>
                            <td>
                                <form action="{{  route('examReg') }}" style="display:inline;">
                                    <button type="submit" class="btn btn-info" data-bs-toggle="popover" data-bs-content="Examenes Registrados">
                                    <i class="fa-solid fa-clipboard-list fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                                <form action="{{  route('editPerson') }}" style="display:inline;">
                                    <button type="submit" class="btn btn-warning" data-bs-toggle="popover" data-bs-content="Editar">
                                        <i class="fa-solid fa-square-pen fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                                <form action="{{  route('prueba') }}" style="display:inline;">
                                    <button type="button" class="btn btn-danger" data-bs-toggle="popover" data-bs-content="Eliminar" onclick="confirmDelete()">
                                        <i class="fa-solid fa-trash fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td>Villavicencio Rodriguez</td>
                            <td>Maria de los Angeles Veronica</td>
                            <td>alexrpnarelle@gmail.com</td>
                            <td>02/02/1999</td>
                            <td>Masculino</td>
                            <td>No</td>
                            <td>Ninguno</td>
                            <td>
                                <form action="{{  route('examReg') }}" style="display:inline;">
                                    <button type="submit" class="btn btn-info" data-bs-toggle="popover" data-bs-content="Examenes Registrados">
                                    <i class="fa-solid fa-clipboard-list fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                                <form action="{{  route('editPerson') }}" style="display:inline;">
                                    <button type="submit" class="btn btn-warning" data-bs-toggle="popover" data-bs-content="Editar">
                                        <i class="fa-solid fa-square-pen fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                                <form action="{{  route('prueba') }}" style="display:inline;">
                                    <button type="button" class="btn btn-danger" data-bs-toggle="popover" data-bs-content="Eliminar" onclick="confirmDelete()">
                                        <i class="fa-solid fa-trash fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td>Villavicencio Rodriguez</td>
                            <td>Maria de los Angeles Veronica</td>
                            <td>alexrpnarelle@gmail.com</td>
                            <td>02/02/1999</td>
                            <td>Masculino</td>
                            <td>Si</td>
                            <td>202998543</td>
                            <td>
                                <form action="{{  route('examReg') }}" style="display:inline;">
                                    <button type="submit" class="btn btn-info" data-bs-toggle="popover" data-bs-content="Examenes Registrados">
                                    <i class="fa-solid fa-clipboard-list fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                                <form action="{{  route('editPerson') }}" style="display:inline;">
                                    <button type="submit" class="btn btn-warning" data-bs-toggle="popover" data-bs-content="Editar">
                                        <i class="fa-solid fa-square-pen fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                                <form action="{{  route('prueba') }}" style="display:inline;">
                                    <button type="button" class="btn btn-danger" data-bs-toggle="popover" data-bs-content="Eliminar" onclick="confirmDelete()">
                                        <i class="fa-solid fa-trash fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- ContentPage -->
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')

</body>
</html>
