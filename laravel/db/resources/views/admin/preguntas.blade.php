<!DOCTYPE html>
<html lang="es">
    
@include('templates.headerPage')

<body>

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>
                <br>
                <header class="sb-page-header">
                    <div class="container">
                        <h1>Lista de preguntas</h1>
                        <p></p>
                    </div>
                </header>
                <br>
                <div class="text-report">
                    <div class="left-link">
                        <a href="{{ route('cuestionario') }}" class="btn btn-primary rounded-pill btn-xs">
                            <i class="fa-solid fa-arrow-left fa-xl" style="color: #ffffff;"></i> Regresar
                        </a>
                    </div>
                    <div class="right-link">
                        <a class="btn btn-success btn-xs" data-dismiss="modal" href="{{ route('newsPregunts') }}" role="button">
                            <i class="fa-solid fa-plus fa-xl" style="color: #ffffff;"></i> Añadir nueva pregunta
                        </a>
                    </div>
                </div>
                 <br><br>
                   <div class="list-group ">
                    <li href="#" class="list-group-item">
                      <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1 p-2">Titulo de la pregunta</h5> 
                        <small class="p-2 ms-auto">22 de mayo al 22 de agosto</small> 


                        <div class="vr"></div>
                        <div class="p-2">                        
                            <form action="{{  route('editPregunts') }}">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-warning" data-bs-toggle="popover" data-bs-content="Editar">
                                        <i class="fa-solid fa-square-pen fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </div>
                            </form>
                        </div>

                        <div class="vr"></div>
                        <div class="p-2">                        
                            <form action="{{  route('prueba') }}">
                                <div class="form-group text-center"> 
                                    <button type="button" class="btn btn-danger" data-bs-toggle="popover" data-bs-content="Eliminar" onclick="confirmDelete()">
                                            <i class="fa-solid fa-trash" fa-xl style="color: #ffffff;"></i>
                                        </button>
                                </div>
                            </form>
                        </div>
                      </div>
                      <p class="mb-1 p-2">Tipo de pregunta </p>
                    </li>
                    <br>
                    
                    <li href="#" class="list-group-item">
                      <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1 p-2">Titulo de la pregunta</h5> 
                        <small class="p-2 ms-auto">22 de mayo al 22 de agosto</small> 
                                      

                        <div class="vr"></div>
                        <div class="p-2">     
                            <form action="{{  route('editPregunts') }}"> 
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-warning" data-bs-toggle="popover" data-bs-content="Editar">
                                        <i class="fa-solid fa-square-pen fa-xl" style="color: #ffffff;"></i>
                                    </button>
                                </div>
                            </form>
                        </div>

                        <div class="vr"></div>
                        <div class="p-2">                        
                            <form action="{{  route('prueba') }}">
                                <div class="form-group text-center">                          
                                    <button type="button" class="btn btn-danger" data-bs-toggle="popover" data-bs-content="Eliminar" onclick="confirmDelete()">
                                            <i class="fa-solid fa-trash" fa-xl style="color: #ffffff;"></i>
                                        </button>
                                </div>
                            </form>
                        </div>
                      </div>
                      <p class="mb-1 p-2">Tipo de pregunta </p>
                    </li>
                    
                </div>
           

                <!-- ContentPage -->
                </div>
            </div>
        </div>
    </div>


    @include('templates.footer')


</body>
</html>
