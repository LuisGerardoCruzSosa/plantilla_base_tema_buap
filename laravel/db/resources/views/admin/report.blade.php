<!DOCTYPE html>
<html lang="es">

@include('templates.headerPage')

<body>

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>
                <header class="sb-page-header">
                    <div class="container">
                        <h1>Bienvenido/a</h1>
                        <p></p>
                    </div>
                </header>
                    
                <div class="row">
                    <div class="panel panel-primary">
                        <div id="page">                            
                            <br><br><br><br><br>
                            
                            <div class="container">
                                <div class="row gy-5">
                                    <div class="col-6">
                                        <div class="p-1">
                                            <div class="card">
                                                <form action="{{ route('personas') }}">
                                                    <div class="form-group text-center">
                                                        <button class="btn btn-default button_report">
                                                            <div class="card-body">
                                                                <i class="fa-solid fa-users fa-2xl" style="color: #ffc800;"></i>
                                                                <br><br>
                                                                Lista de personas
                                                            </div>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="p-1">
                                            <div class="card">
                                                <form action="{{ route('examenes') }}">
                                                    <div class="form-group text-center">
                                                        <button class="btn btn-default button_report">
                                                            <div class="card-body">
                                                                <i class="fa-regular fa-rectangle-list fa-2xl" style="color: #ffc800;"></i>
                                                                <br><br>
                                                                Lista de examenes
                                                            </div>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')

</body>
</html>
