<!DOCTYPE html>
<html lang="es">
<head>

    @include('templates.headerPage') 
</head>
<body>
    @yield('content')

     @include('templates.footer') 
</body>
</html>
