<script type="text/javascript" src="{{ asset('resources/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/boostrap/js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/DataTables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/js/moment-with-locales.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/js/app_script.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/sweetalert/sweetalert2.all.min.js') }}"></script>
