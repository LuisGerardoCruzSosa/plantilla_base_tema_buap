<link rel="icon" type="image/png" href="{{ asset('resources/images/favicon.png') }}">
<link type="text/css" href="{{ asset('resources/css/normalize.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('resources/fontawesome-web/css/all.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('resources/boostrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('resources/css/app_style.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('resources/css/test01.css') }}" rel="stylesheet">
