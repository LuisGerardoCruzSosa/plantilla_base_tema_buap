<!-- Footer -->
<div id="footerPage" class="container-fluid py-4">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-2 col-xl-2">
                        <img class="img-fluid d-block mx-auto mb-4" src="{{ asset('resources/images/buap_footer_bottom.png') }}" alt="BUAP">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
                        <p class="small text-buap-light text-center text-sm-center text-md-left text-lg-left text-xl-left">
                            <strong>Benemérita Universidad Autónoma de Puebla</strong><br>
                            4 Sur 104 Centro Histórico C.P. 72000<br>
                            Teléfono +52 (222) 229 55 00
                        </p>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
                        <ul class="list-unstyled text-center text-sm-center text-md-left text-lg-left text-xl-left">
                            <li>
                                <a class="text-decoration-none small text-buap-light" href="http://www.transparencia.buap.mx/" target="_blank">Transparencia y Acceso a la Información</a>
                            </li>
                            <li>
                                <a class="text-decoration-none small text-buap-light" href="https://consultapublicamx.inai.org.mx/vut-web/?idSujetoObigadoParametro=4479&idEntidadParametro=21&idSectorParametro=24" target="_blank">Obligaciones de Transparencia</a>
                            </li>
                            <li>
                                <a class="text-decoration-none small text-buap-light" href="http://www.pdi.buap.mx/" target="_blank">PDI 2017 - 2021</a>
                            </li>
                            <li>
                                <a class="text-decoration-none small text-buap-light" href="http://www.buap.mx/privacidad" target="_blank">Aviso de Privacidad</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                        <p class="small text-buap-light text-center text-sm-center text-md-left text-lg-left text-xl-left">
                            <strong>Dirección de Acompañamiento Universitario</strong><br>
                            Torre de Gestión Académica y Servicios Administrativos<br>
                            Ciudad Universitaria.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
