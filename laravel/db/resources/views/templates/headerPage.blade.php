<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Psicométrico Posgrado</title>
    <!-- Favicon -->

    @include('resources.styles') 
    @include('resources.scripts') 

</head>

<nav class="navbar navbar-expand-lg py-4px px-lg-5" data-bs-theme="dark">
  <div class="container-fluid px-lg-5 mx-lg-6">
    <div class="d-inline-block me-5 me-lg-0"></div>
    <a class="navbar-brand disabled py-0" href="{{ route('userIndex') }}">
      <img class="d-inline-block align-text-top" height="63" src="{{ asset('resources/images/buap_header_top.png') }}" alt="BUAP">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{ route('userIndex') }}">
            <i class="fas fa-home"></i> Inicio 
          </a>
        </li>
        
        <li class="nav-item">
          @if(session()->has('login') && session('login') == true)
            <a class="nav-link" href="{{ route('adminlogout') }}">
              <i class="fas fa-sign-out-alt"></i>
              Cerrar sesión
            </a>
          @else
            <a class="nav-link" href="{{ route('adminlogin') }}">
              <i class="fa-solid fa-right-to-bracket"></i>
              Acceso
            </a>
          @endif
        </li>

        <li class="nav-item dropdown" hidden>
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul class="dropdown-menu bg-navbar">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
