<!DOCTYPE html>
<html lang="es">
     
@include('templates.headerPage')

<body>

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>

                <h1 class="text-center mb-5" id="title-Aviso">Aviso de Privacidad</h1>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="modal-content">
                                <div class="space-hidden-content text-center bg-white rounded-3" id="text-Aviso">
                                    <iframe class="pdf-container" src="http://dau.buap.mx/sites/default/files/aviso-privacidad.pdf"></iframe>
                                    <br><br>
                                </div>
                                
                                <div>
                                    <div class="text-center ">
                                        <a href="{{ route('userIndex') }}" class="btn btn-danger btn-lg" data-dismiss="modal">
                                            <i class="fa-regular fa-times-circle"></i>
                                           Rechazar
                                        </a>
                                        <a class="btn btn-success btn-lg " data-dismiss="modal" id="aceptar" href="{{ route('registro', ['idExamen' => $idExamen]) }}" role="button">
                                            <i class="fa-regular fa-circle-check"></i>
                                           Aceptar
                                        </a>
                                    </div>
                                </div>
                                <br/>
                                
                            </div>
                        </div>
                    </div>
                <br>
            </div>
        </div>
    </div>
</div>

    @include('templates.footer')

</body>
</html>
