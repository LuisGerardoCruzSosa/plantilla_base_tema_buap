<!DOCTYPE html>
<html lang="es">

@include('templates.headerPage')

<body>
    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>


        <div class="container">
          <div class="row" >
            <div class="col-xs-12">
              <div class="panel panel-default" >
                <div class="list-group-item text-justify">
            		<form class="hide-radio" id="msform" method="POST" onSubmit="return validarEncuesta()"  action="{{ route('section.submit', ['section_id' => $section->id]) }}">
						@csrf
								<h3>
                                    {!! $section->description_section !!} 
                                </h3>
							<table class="table table-condensed table-hover">
							@foreach ($section->questions as $question)
							    <tr>
								        <div class="col-lg-12 ">
								            <div class="col-lg-12">
												<h4><strong>{{ $loop->iteration }}.</strong> {{ $question->question_text }}</h4>
								            </div>
								        		<div class="col-xs-10">
								    				<div data-toggle="buttons">
														<br>
														@if ($question->typeQuestion->type_name === 'multiple')
														@foreach ($question->answers as $answer)
															<label class="btn btn-default button_resp">
																<input type="radio" name="question-{{ $question->id }}" value="{{ $answer->id }}" > {{ $answer->answer_text }}
															</label>
															@endforeach
												    </div>
												</div>
								        </div>
				                </tr>
				                <tr>
										<div class="col-md-12">
											<br>
											@elseif ($question->typeQuestion->type_name === 'abierto')
												<div class="col-lg-10 form-group">
													<textarea name="question-{{ $question->id }}" class="form-control" rows="3"></textarea>
												</div>
											@endif
										</div>
								</tr>
								@endforeach
								</table>
                                <br>
								<div class="form-group text-center">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa-solid fa-arrow-right"></i>
                                              Siguiente
                                        </button>
                                </div>		
                                      
                                		</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
	

</body>
</html>
