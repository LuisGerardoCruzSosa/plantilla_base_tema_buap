<!DOCTYPE html>
<html lang="es">
    
@include('templates.headerPage')

<body>

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>

                <h1 class="text-center mb-5">Examen psicométrico</h1>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="line-he text-justify">Las pruebas psicométricas son herramientas utilizadas por el personal de psicología, para conocer aspectos de la personalidad de quienes se evalúan, ya sea por motivos de salud, escolares y/o laborales, entre otros. La Dirección de Acompañamiento Universitario, propone este conjunto de pruebas psicométricas para las y los estudiantes, a petición de la Unidad Académica. Para responderlas, te sugerimos lo siguiente:<br>
                                <ul class="text-justify" style="margin-top: 10px; margin-bottom: 10px;">
                                    <li>Realiza tu prueba en un equipo de computo (PC de escritorio) con conexión a red (internet) y corriente eléctrica estable, no te recomendamos utilizar dispositivos móviles <strong style="color: red;">(No laptops, tabletas y/o teléfonos celulares)</strong>.</li>
                                    <li>La prueba solo se podrá realizar una vez, en caso de no concluirla, el sistema no te permitirá volver a ingresar, por lo que te recomendamos revisar tu conexión a internet.</li>
                                    <li>Lee las instrucciones y asegúrate de comprenderlas, responde la totalidad de los reactivos, otorgándole la importancia que merecen.</li>
                                    <li>Las respuestas no requieren pensarse demasiado, contesta con honestidad y sin pensar en respuestas buenas o malas.</li>
                                    <li>Para responder la prueba, te recomendamos estar sobre un espacio cómodo, sin compañía preferentemente, libre de ruido e interrupciones y con el tiempo necesario para las pruebas, de 30 minutos a 1 hora en promedio.</li>
                                    <li>Evita pensamientos negativos, se requiere autenticidad de tu persona para responder, es decir, que seas tú misma (o).</li>
                                </ul>
                                La DAU, agradece tu disposición para la realización de este paso.
                            </h4>
                        </div>
                    </div>
                    <br>
                    <div class="text-center">
                        <a class="btn btn-success btn-lg" href="{{ route('listas') }}" role="button"><i class="fa-solid fa-check"></i> Comenzar prueba</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')

</body>
</html>
