<!DOCTYPE html>
<html lang="es">

@include('templates.headerPage')

<body>
    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>

                <h1 class="text-center mb-5">Listas Examen</h1>

                <div class="list-group">
                @foreach($exams as $exam)
                    <li class="list-group-item">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{ $exam->title }}</h5>
                            <small><b>{{ $exam->dateStart }} </b> al <b>{{ $exam->dateEnd }}</b></small>
                        </div>
                        <p class="mb-1">{{ $exam->description }}</p>
                        <a class="btn btn-sn btn-success" href="{{ route('avisoPrivacidad', ['idExamen' => $exam->id]) }}">
                            <i class="fa-solid fa-hand-point-right"></i>
                            Iniciar examen
                        </a>
                    </li>
                    <br>
                @endforeach
                </div>
                
            </div>
        </div>
    </div>

    @include('templates.footer')

</body>
</html>
