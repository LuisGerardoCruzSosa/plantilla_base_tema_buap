<!DOCTYPE html>
<html lang="es">

@include('templates.headerPage')

<body>  
    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>
                <header class="sb-page-header">
                    <div class="container">
                        <div class="col-lg-12">
                            <h2 class="page-header">La información proporcionada ha sido almacenada satisfactoriamente<br />¡Gracias por participar en este importante proceso!</h2>
                        </div>
                    </div>
                </header>
                <!-- Page Content -->
                <div class="container text-center">
                    <div class="row">
                        <div>
                            <br><br><br><br>
                            <h2>¿Le gustaria participar en la encuesta?</h2>
                        </div>
                        <div><br><br>
                                    <div class="text-center ">
                                      <a  href="{{ route('userIndex') }}" class="btn btn-danger btn-lg" data-dismiss="modal">Rechazar</a>
                                      <a class="btn btn-success btn-lg " data-dismiss="modal" id="aceptar" href="{{ route('encuesta') }}" role="button">Aceptar</a>
                                    </div>
                                </div>
                    </div>

                </div>


            </div>
        </div>
    </div>

    @include('templates.footer')

</body>
</html>
