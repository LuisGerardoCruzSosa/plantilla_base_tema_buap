<!DOCTYPE html>
<html lang="es">

@include('templates.headerPage')

<body>  
    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>
                <br><br><br><br>
                <header class="sb-page-header">
                    <div class="container">
                        <div class="col">
                            <h2 class="page-header"><br>¡Gracias por participar en este importante proceso!</h2>
                        </div>
                    </div>
                </header>
                <!-- Page Content -->
                <div class="container text-center">


                </div>


            </div>
        </div>
    </div>
    
    @include('templates.footer')

</body>
</html>
