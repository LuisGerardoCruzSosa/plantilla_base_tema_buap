<!DOCTYPE html>
<html lang="es">

@include('templates.headerPage')

<body>
    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>
    
                    <div class="container">
                        <div class="row" >
                            <div class="col-xs-12">
                                <div class="panel panel-default" >
                                    <div class="list-group-item">
                                    <form class="hide-radio" id="msform" method="POST" onSubmit="return validarFormulario()" action="{{ route('section.submit', ['section_id' => $section->id]) }}">
                                        @csrf
                                        
                                        <h4>
                                            <strong>Instrucciones: </strong><br>
                                                        {!! $section->description_section !!} 
                                            <br><br>
                                        </h4>
                                        <table class="table table-condensed table-hover">
                                            @foreach ($section->questions as $question)
                                                <tr>
                                                        <div class="col-lg-15 mx-auto">
                                                            <div class="col-xs-12">
                                                                <h4><strong>{{ $loop->iteration }}.</strong> {{ $question->question_text }}</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 mx-auto ">
                                                            <div data-toggle="buttons">



                                                    @if ($question->typeQuestion->type_name === 'multiple')
                                                    @foreach ($question->answers as $answer)
                                                        <label class="btn btn-default button_resp">
                                                            <input type="radio" name="question-{{ $question->id }}" value="{{ $answer->id }}" autocomplete="off"> {{ $answer->answer_text }}
                                                        </label>
                                                    @endforeach
                                                    </div>
                                                </div>
                                                <br>
                                                <tr>
                                                    <div class="col-md-12">
                                                @elseif ($question->typeQuestion->type_name === 'abierto')
                                                    <div class="col-lg-10 form-group">
                                                        <textarea name="question-{{ $question->id }}" class="form-control" rows="3"></textarea>
                                                    </div>
                                                @elseif ($question->typeQuestion->type_name === 'radio')
                                                    <div class="col-lg-10 form-group">
                                                        @foreach ($question->answers as $answer)
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="question-{{ $question->id }}" value="{{ $answer->id }}" autocomplete="off"> {{ $answer->answer_text }}
                                                                </label>
                                                            </li><br>
                                                        @endforeach
                                                    </div>
                                                @endif
                                                </div>
                                                </tr>
                                                @endforeach


                                    </table>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa-solid fa-arrow-right"></i>
                                              Siguiente
                                        </button>
                                    </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    @include('templates.footer')

    <script type="text/javascript" src="{{ asset('resources/js/validar_test.js') }}"></script>
    
</body>
</html>

