<!DOCTYPE html>
<html lang="es">

@include('templates.headerPage')

<body>
    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>
                    <div class="container">
                        <div class="row" >
                            <div class="col-lg-12">
                                <div class="panel panel-default" >
                                    <div class="list-group-item text-justify ">

                                    <form class="hide-radio" id="msform" method="POST" onSubmit="return validarFormulario()" action="{{ route('section.submit', ['section_id' => $section->id]) }}">
                                    @csrf
                                            <h4>
                                                <strong>Instrucciones: </strong><br><br>
                                                    <ul>
                                                        <li> 
                                                            {!! $section->description_section !!} 
                                                        </li>
                                                    </ul>
                                            </h4>
                                            <table class="table table-condensed table-hover">
                                                @foreach ($section->questions as $question)            
                                                <tr>
                                                    <td>
                                                        <div class="col-lg-15 mx-auto">
                                                            <div class="col-xs-12">
                                                                <h4><strong>{{ $loop->iteration }}.</strong> {{ $question->question_text }}</h4>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <div class="d-flex flex-wrap" data-toggle="buttons mx-auto">
                                                                @foreach ($question->answers as $answer)
                                                                    <label class="btn btn-default button_resp">
                                                                        <input type="radio" name="question-{{ $question->id }}" value="{{ $answer->id }}" autocomplete="off"> {{ $answer->answer_text }}
                                                                    </label>
                                                                    @endforeach
                                                            <div class="col-md-10">
                                                                <div  data-toggle="buttons">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                         
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa-solid fa-arrow-right"></i>
                                                   Siguiente
                                                </button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    @include('templates.footer')


    <script type="text/javascript" src="{{ asset('resources/js/validar_test.js') }}"></script>

</body>
</html>
