<!DOCTYPE html>
<html lang="es">
    
@include('templates.headerPage')

<body>

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container">
                <form id="msform" method="POST" onSubmit="return validarFormulario4_L2()"  action="{{ route('section.submit', ['section_id' => $section->id]) }}">
                @csrf
                                    <h4>           
                                        <strong>Instrucciones: <br>
                                                {!! $section->description_section !!} 
                                        </strong><br><br>
                                    </h4>
									
									<table class="table table-condensed table-hover">
                                    @foreach ($section->questions as $question)  
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                   <h4><strong>{{ $loop->iteration }}.</strong> {{ $question->question_text }}</h4>
                                                </div>
                                                <div class="col-md-10"><ul class="list-unstyled"><strong>
                                                @foreach ($question->answers as $answer)
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="question-{{ $question->id }}" value="{{ $answer->id }}" autocomplete="off"> {{ $answer->answer_text }}
                                                        </label>
                                                    </li><br>
                                                    @endforeach
                                                </strong></ul></div>
                                                </div>
                                                @endforeach
									</table>
                                    <div class="form-group text-center">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa-solid fa-arrow-right"></i>
                                                   Siguiente
                                                </button>
                                    </div>
						</form>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')

</body>
</html>
