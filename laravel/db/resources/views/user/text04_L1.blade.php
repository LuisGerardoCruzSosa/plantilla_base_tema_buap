<!DOCTYPE html>
<html lang="es">
    
@include('templates.headerPage')

<body>



    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-body">
                            <form  action="{{ route('test04Lectura1') }}">
                                    <h4>
                                        <strong>Instrucciones: </strong><br><br>
                                        <h4><strong> {!! $section->description_section !!} </strong></h4>
                                    </h4>
                                                <div><h4>El tiempo restante es: <strong> <span id="time"></span></strong></h4></div>
                                                <hr>
                                                <h4 style="text-align: justify;">{!! $lectura->texto_lectura !!}</h4>
                                                <hr>
                                        </div>
                                        <div class="form-group text-center">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa-solid fa-arrow-right"></i>
                                                   Siguiente
                                                </button>
                                        </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')

</body>
</html>
