<!DOCTYPE html>
<html lang="es">
    
@include('templates.headerPage')

<body>

<script>
       
       function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            var interval = setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.textContent = minutes + ":" + seconds;

            localStorage.setItem('remainingTime', timer);

                if (--timer < 0) {
                    clearInterval(interval);

                    localStorage.removeItem('remainingTime');

                    window.location.href = '/db/public/user/test04_L2';
                }
            }, 1000);
        }

        window.onload = function () {
            var display = document.querySelector('#time');
            var storedTime = localStorage.getItem('remainingTime');

            var setMinutes = 60 * 1;
            var remainingTime = storedTime ? parseInt(storedTime, 10) : setMinutes;

            startTimer(remainingTime, display);
        };


        window.addEventListener('pageshow', function (event) {
            if (event.persisted) {
                Swal.fire({
                    icon: "warning",
                    title: "Oops...",
                    text: "Has regresado a la pagina, por favor continua con las preguntas",
                    confirmButtonText: "Aceptar"
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = '/db/public/user/test04_L2';
                    }
                });
            }            
        });
    </script> 

    <div id="mainContentPage" class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                <div class="container-fluid mb-3">
                    <div class="row justify-content-center justify-content-lg-end">
                        <div class="col-12 col-sm-12 col-md-10 col-lg-6 col-lg-4">
                            <h4 id="title-DAU" class="fw-bold text-buap-dark text-center p-2">Dirección de Acompañamiento Universitario</h4>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-body">
                        <form  action="{{ route('test04Lectura2') }}">
                                    <h4>
                                        <strong>Instrucciones: </strong><br><br>
                                        <h4><strong> {!! $section->description_section !!} </strong></h4>
                                    </h4>
									        <div><h4>El tiempo restante es: <strong> <span id="time"> 01:00</span></strong></h4></div>
                                            <hr>
                                                <!-- @foreach($lecturas as $lectura) -->
                                                    <!-- <h4 style="text-align: center;"><strong>{{ $lectura->titulo_lectura ?? 'Lectura' }}</strong></h4> -->
                                                    <h4 style="text-align: justify;">{!! $lectura->texto_lectura !!}</h4>
                                                    <br>
                                                <!-- @endforeach -->
                                            <hr>
									
                                    <div class="form-group text-center">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa-solid fa-arrow-right"></i>
                                                   Siguiente
                                                </button>
                                        </div>
						</form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')

</body>
</html>
