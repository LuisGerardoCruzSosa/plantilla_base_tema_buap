<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LecturaController;
use App\Http\Controllers\AdminLoginController;
use App\Http\Controllers\RegistroController;
use App\Http\Controllers\ExamController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\TestController;
use App\Models\Exam;
use App\Models\Section;
use App\Models\Lectura;


Route::get('/', function () {
    return view('welcome');
})->name('home');


//Usuario
Route::get('/user', function () {
    return view('user/index');
})->name('userIndex');


Route::get('/user/listas', function () {
    $exams = Exam::all();
    return view('user/listas', compact('exams'));
})->name('listas');


Route::get('/user/aviso_privacidad/{idExamen}', function ($idExamen) {
    return view('user/aviso_privacidad', compact('idExamen'));
})->name('avisoPrivacidad');


Route::get('/user/registro/{idExamen}', function ($idExamen) {
    return view('user/registro', compact('idExamen'));
})->name('registro');

//Post registro
Route::post('/registro/{idExamen}', [RegistroController::class, 'usuario'])->name('registro.usuario');

Route::get('/user/section/{section_id}', function ($section_id) {
    $section = Section::with('questions')->find($section_id);

    if ( $section != null ) {
        if($section_id == 5){
            return view('user/test04_L1', compact('section'));
        }
        $lectura = Lectura::where('section_id', $section_id)->first();

        if ( $lectura != null ) 
            return view('user/text04_L1', compact('section', 'lectura') );

        else
            return view('user/test01', compact('section'));
        
    }

    return "Seccion no valida";

})->name('section');

Route::post('/user/section/{section_id}/submit', function ($section_id) {
    $nextSectionId = $section_id + 1;
    return redirect()->route('section', ['section_id' => $nextSectionId]);
})->name('section.submit');


Route::get('/user/test01', function () {
    $section = Section::find(1);     
    return view('user/test01', compact('section'));
})->name('test01');

Route::get('/user/test02', function () {
    $section = Section::find(2);     
    return view('user/test02', compact('section'));
})->name('test02');

Route::get('/user/test03', function () {
    $section = Section::find(3);     
    return view('user/test03', compact('section'));
})->name('test03');

Route::get('/user/test05', function () {
    $section = Section::find(6);     
    return view('user/test05', compact('section'));
})->name('test05');

Route::get('/user/test06', function () {
    $section = Section::find(7);     
    return view('user/test06', compact('section'));
})->name('test06');


//Lectura 1
Route::get('/user/text04_L1', function () {
    $section = Section::find(4);
    $lecturas = Lectura::where('section_id', 7)->get();
    return view('user/text04_L1', compact('section', 'lecturas'));
})->name('text04Lectura1');

Route::get('/user/test04_L1', function () {
    $section = Section::find(5);     
    return view('user/test04_L1', compact('section'));
})->name('test04Lectura1');


//Lectura 2
Route::get('/user/text04_L2', function () {
    $section = Section::find(9);
    $lecturas = Lectura::where('section_id', 7)->get();
    return view('user/text04_L2', compact('section', 'lecturas'));
})->name('text04Lectura2');

Route::get('/user/test04_L2', function () {
    $section = Section::find(10);     
    return view('user/test04_L2', compact('section'));
})->name('test04Lectura2');

Route::get('/user/out', function () {
    return view('user/out');
})->name('out');

Route::get('/user/out2', function () {
    return view('user/out2');
})->name('out2');


//Post Lectura 1 o Lectura 2
Route::post('/procesaLectura', [LecturaController::class, 'procesaLectura'])->name('procesaLectura');


//Encuesta
Route::get('/user/encuesta', function () {
    $section = Section::find(8);
    return view('user/encuesta', compact('section'));
})->name('encuesta');

//Administrador Acceso
Route::get('/admin', [AdminLoginController::class, 'showLoginForm'])->name('adminlogin');
Route::post('/adminlogin', [AdminLoginController::class, 'loginadmin'])->name('adminlogin.post');
Route::get('adminlogout', [AdminLoginController::class, 'logoutadmin'])->name('adminlogout');
Route::get('password', function() {
    dd(bcrypt('asd'));
});

//Administrador
Route::get('/admin', function () {
    return view('admin/acceso');
})->name('adminlogin');

Route::get('/admin/report', function () {
    return view('admin/report');
})->name('report');

Route::get('/admin/personas', function () {
    return view('admin/personas');
})->name('personas');

Route::get('/admin/examReg', function () {
    return view('admin/examReg');
})->name('examReg');

Route::get('/admin/editPerson', function () {
    return view('admin/editPerson');
})->name('editPerson');


//Examenes
Route::get('/admin/examenes', function () {
    return view('admin/examenes');
})->name('examenes');

Route::get('/admin/nueExam', function () {
    return view('admin/nueExam');
})->name('newExam');

Route::get('/admin/personReg', function () {
    return view('admin/personReg');
})->name('personReg');

Route::get('/admin/prueba', function () {
    return view('admin/prueba');
})->name('prueba');

Route::get('/admin/editExam', function () {
    return view('admin/editExam');
})->name('editExam');

Route::get('/admin/LPeditExam', function () {
    return view('admin/LPeditExam');
})->name('LPeditExam');


//Cuestionarios
Route::get('/admin/cuestionario', function () {
    return view('admin/cuestionario');
})->name('cuestionario');

Route::get('/admin/nueCuest', function () {
    return view('admin/nueCuest');
})->name('nueCuest');

Route::get('/admin/newsPregunts', function () {
    return view('admin/newsPregunts');
})->name('newsPregunts');

Route::get('/admin/editCuestionario', function () {
    return view('admin/editCuestionario');
})->name('editCuestionario');

Route::get('/admin/editPregunts', function () {
    return view('admin/editPregunts');
})->name('editPregunts');

Route::get('/admin/preguntas', function () {
    return view('admin/preguntas');
})->name('preguntas');


