<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testWelcomedView()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertViewIs('welcome');
    }

    public function testUserView()
    {
        $response = $this->get('/user');

        $response->assertStatus(200);
        $response->assertViewIs('user.dashboard');
    }
}
