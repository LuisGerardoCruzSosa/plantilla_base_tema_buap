-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-09-2024 a las 20:58:58
-- Versión del servidor: 10.4.32-MariaDB
-- Versión de PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_psicometrico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `answer_text` varchar(255) NOT NULL,
  `questions_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `answers`
--

INSERT INTO `answers` (`id`, `answer_text`, `questions_id`) VALUES
(1, 'Verdadero', 1),
(2, 'Falso', 1),
(3, 'no en absoluto', 4),
(4, 'un poco', 4),
(5, 'mucho', 4),
(6, 'muchisimo', 4),
(7, 'Verdadero', 2),
(8, 'Falso', 2),
(9, 'Verdadero', 3),
(10, 'Falso', 3),
(11, 'Verdadero', 24),
(12, 'Falso', 24),
(13, 'Verdadero', 25),
(14, 'Falso', 25),
(15, 'no en absoluto', 5),
(16, 'un poco', 5),
(17, 'mucho', 5),
(18, 'muchisimo', 5),
(19, 'no en absoluto', 6),
(20, 'un poco', 6),
(21, 'mucho', 6),
(22, 'muchisimo', 6),
(23, 'no en absoluto', 7),
(24, 'un poco', 7),
(25, 'mucho', 7),
(26, 'muchisimo', 7),
(27, 'no en absoluto', 8),
(28, 'un poco', 8),
(29, 'mucho', 8),
(30, 'muchisimo', 8),
(31, 'Nunca', 27),
(32, 'Casi Nunca', 27),
(33, 'A veces', 27),
(34, 'Frecuentemente', 27),
(35, 'Casi siempre', 27),
(36, 'Siempre', 27),
(37, 'Nunca', 28),
(38, 'Casi Nunca', 28),
(39, 'A veces', 28),
(40, 'Frecuentemente', 28),
(41, 'Casi siempre', 28),
(42, 'Siempre', 28),
(43, 'Nunca', 29),
(44, 'Casi Nunca', 29),
(45, 'A veces', 29),
(46, 'Frecuentemente', 29),
(47, 'Casi siempre', 29),
(48, 'Siempre', 29),
(55, 'Casi nunca', 31),
(56, 'Algunas veces', 31),
(57, 'A menudo', 31),
(58, 'Casi siempre', 31),
(59, 'Casi nunca', 32),
(60, 'Algunas veces', 32),
(61, 'A menudo', 32),
(62, 'Casi siempre', 32),
(63, 'Casi nunca', 33),
(64, 'Algunas veces', 33),
(65, 'A menudo', 33),
(66, 'Casi siempre', 33),
(71, 'Muy en desacuerdo', 35),
(72, 'En parte en desacuerdo', 35),
(73, 'Ni de acuerdo ni en desacuerdo', 35),
(74, 'En parte de acuerdo', 35),
(75, 'Muy de acuerdo', 35),
(76, 'Muy en desacuerdo', 36),
(77, 'En parte en desacuerdo', 36),
(78, 'Ni de acuerdo ni en desacuerdo', 36),
(79, 'En parte de acuerdo', 36),
(80, 'Muy de acuerdo', 36),
(81, 'Muy en desacuerdo', 37),
(82, 'En parte en desacuerdo', 37),
(83, 'Ni de acuerdo ni en desacuerdo', 37),
(84, 'En parte de acuerdo', 37),
(85, 'Muy de acuerdo', 37),
(91, ' \"una justificación de la aplicación indiscriminada de vacunas a toda población.\"', 39),
(92, '     \"una breve historia de los problemas de la vacunación en tiempos modernos.\",', 39),
(93, '\"un alegato para descalificar el empleo de vacunas en los niños más pequeños.\",', 39),
(94, '\"una descalificación de los efectos secundarios en la aplicación de las vacunas.\"', 39),
(95, '\"promueven el uso indiscriminado de vacunas en la infancia.\",', 40),
(96, '\"emplean investigadores médicos para defender su postura.\",', 40),
(97, '  \"subvaloran los peligros de enfermedades como el sarampión.\",', 40),
(98, '\"dudan de que la composición interna de las vacunas sea perjudicial.\",', 40),
(99, '\"se pondría en riesgo la salud de millones de niños en el mundo.\",', 41),
(100, '\"se multiplicarían los casos de algunas enfermedades congénitas.\",', 41),
(101, '\"los médicos combatirían los efectos secundarios de la vacunación.\",', 41),
(102, ' \"las enfermedades infecciosas empezarían a declinar en el mundo.\",', 41),
(103, '\"la pertinencia de seguir empleando la categoría de clase social para explicar la forma de alimentación actual. \",', 42),
(104, ' \"la creciente desestructuración de los hábitos alimentarios en las sociedades europeas modernas.\",', 42),
(105, '\"los conceptos que se emplean para comprender y explicar los modelos alimentarios actuales.\",', 42),
(106, 'un mecanismo mediante el cual las clases sociales acomodadas manipulan a las clases bajas.\",', 43),
(107, '\"una estrategia por la cual algunos individuos buscan ascender dentro de la pirámide social.\",', 43),
(108, '\"una manifestación de cierta insatisfacción que sienten algunos sujetos consigo mismos.\",', 43),
(109, '\"muestran sus ideas, sobre todo, en los segundos párrafos.\",', 44),
(110, '\"presentan argumentos contrastados con evidencia empírica.\",', 44),
(111, ' \"pretenden avalar sus posiciones mediante el uso de encuestas.\",', 44),
(112, 'Completamente satisfecho', 45),
(113, 'Satisfecho', 45),
(114, 'Insatisfecho', 45),
(115, 'Completamente insatisfecho', 45),
(116, 'Excelente', 47),
(117, 'Regular', 47),
(118, 'Malo', 47),
(120, 'Excelente', 48),
(121, 'Regular', 48),
(122, 'Malo', 48),
(123, 'Excelente', 49),
(124, 'Regular', 49),
(125, 'Malo', 49);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers_responses`
--

CREATE TABLE `answers_responses` (
  `id` int(11) NOT NULL,
  `answers_id` int(11) NOT NULL,
  `answers_questions_id` int(11) NOT NULL,
  `register_id` int(11) NOT NULL,
  `register_exams_id` int(11) NOT NULL,
  `register_gender_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `answers_responses`
--

INSERT INTO `answers_responses` (`id`, `answers_id`, `answers_questions_id`, `register_id`, `register_exams_id`, `register_gender_id`) VALUES
(1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cache`
--

CREATE TABLE `cache` (
  `key` varchar(255) NOT NULL,
  `value` mediumtext NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cache_locks`
--

CREATE TABLE `cache_locks` (
  `key` varchar(255) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuesta`
--

CREATE TABLE `encuesta` (
  `id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `register_id` int(11) NOT NULL,
  `register_user_id` int(11) NOT NULL,
  `register_exams_id` int(11) NOT NULL,
  `register_gender_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `section_exams_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `encuesta`
--

INSERT INTO `encuesta` (`id`, `created_at`, `user_id`, `register_id`, `register_user_id`, `register_exams_id`, `register_gender_id`, `section_id`, `section_exams_id`) VALUES
(1, '2024-07-01', 2, 1, 2, 1, 1, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exams`
--

CREATE TABLE `exams` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(450) NOT NULL,
  `dateStart` date NOT NULL,
  `dateEnd` date NOT NULL,
  `active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `exams`
--

INSERT INTO `exams` (`id`, `title`, `description`, `dateStart`, `dateEnd`, `active`) VALUES
(1, 'titulo del examen', 'descripcion del examen', '2024-07-01', '2024-07-31', 1),
(2, 'titulo del examen 2', 'descripcion del examen 2', '2024-07-21', '2024-07-31', 1),
(3, 'titulo del examen 3', 'descripcion del examen 3', '2024-07-01', '2024-07-06', 1),
(6, 'Titulo del examen de prueba', 'Decripción del examen', '2015-08-28', '2019-08-28', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filtrer_logs`
--

CREATE TABLE `filtrer_logs` (
  `id` int(11) NOT NULL,
  `content` varchar(250) NOT NULL,
  `logs_id` int(11) NOT NULL,
  `logs_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `filtrer_logs`
--

INSERT INTO `filtrer_logs` (`id`, `content`, `logs_id`, `logs_user_id`) VALUES
(1, 'content filtrer', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `gender` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `gender`
--

INSERT INTO `gender` (`id`, `gender`) VALUES
(0, 0),
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) NOT NULL,
  `payload` longtext NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `job_batches`
--

CREATE TABLE `job_batches` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `total_jobs` int(11) NOT NULL,
  `pending_jobs` int(11) NOT NULL,
  `failed_jobs` int(11) NOT NULL,
  `failed_job_ids` longtext NOT NULL,
  `options` mediumtext DEFAULT NULL,
  `cancelled_at` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `finished_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lecturas`
--

CREATE TABLE `lecturas` (
  `id` int(11) NOT NULL,
  `texto_lectura` longtext NOT NULL,
  `tiempo` time NOT NULL,
  `section_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `lecturas`
--

INSERT INTO `lecturas` (`id`, `texto_lectura`, `tiempo`, `section_id`) VALUES
(1, '                                                <h4 style=\"text-align: center;\"><strong>Texto A</strong></h4>\r\n                                                <h4 style=\"text-align: justify;\">Después de escuchar, debatir, leer libros y revisar evidencias y argumentos a favor y en contra de las vacunas, tanto de líderes antivacunas como de epidemiólogos, inmunólogos, médicos de familia y pediatras, mi posición es claramente a favor de las vacunas incluidas en el calendario vacunal.</h4>\r\n                                                <h4 style=\"text-align: justify;\">Pero también soy firme defensor de que las vacunas han de ser las justas y no más. Las vacunas son extremadamente seguras, pero hay que tener en cuenta que, aunque es infrecuente, las vacunas, como cualquier medicamento, pueden tener efectos secundarios: la mayoría leves, alguno grave (1 por 100 000 a 1 por millón de dosis) y, excepcionalmente, alguno muy grave (menos de 1 por cada 1-10 millones de dosis). Estas complicaciones también ocurren cuando se pasan las enfermedades de forma natural, pero con una frecuencia muchísimo mayor, de manera que el beneficio siempre compensa con creces el extremadamente bajo riesgo de vacunarse.</h4>\r\n                                                <h4 style=\"text-align: justify;\">El principal motivo que algunas personas tienen para no vacunarse es la desconfianza en la seguridad de las vacunas. Las clásicas y más benéficas, como las de la difteria, tétanos, tos ferina o polio, llevan años usándose y han sido sometidas a un escrutinio de efectos secundarios muy estricto.</h4>\r\n                                                <h4 style=\"text-align: justify;\">Dejar de vacunar a un niño lo pone en riesgo a él y al resto de su comunidad, especialmente a las personas más debilitadas y no vacunadas por cuestiones de contraindicación o de dificultad de acceso a la sanidad, y dificulta la labor de erradicar enfermedades del planeta.</h4>\r\n                                                <br>\r\n                                                <h4 style=\"text-align: center;\"><strong>Texto B</strong></h4>\r\n                                                <h4 style=\"text-align: justify;\">En la historia de la medicina moderna, han ocurrido algunos hechos que desencadenaron el miedo a las vacunas en algunos círculos sociales. Existe una serie de argumentos de estos grupos para oponerse o relativizar el uso de vacunas.</h4>\r\n                                                <h4 style=\"text-align: justify;\">Por ejemplo, en 1998 se publicó un estudio en la revista médica The Lancet que relacionaba la vacuna de la triple vírica con el autismo. Posteriormente se supo que su autor había falseado los datos para hacer parecer que existía tal asociación cuando no era así; sin embargo, los temores sobrevivieron. Otros casos de brotes de enfermedad por vacunas defectuosas se remontan a cuando comenzaba la era de las vacunas en los siglos xviii y xix, con la viruela, o en la primera mitad del siglo xx, con otras vacunas. En estos casos, las vacunas produjeron efectos secundarios negativos en parte significativa de la población vacunada.</h4>\r\n                                                <h4 style=\"text-align: justify;\">Aunque siempre se podrán mejorar los protocolos de aplicación y la composición misma de las vacunas, la mayoría de estas acusaciones de efectos secundarios perniciosos no se sostienen Entre estos falsos efectos secundarios se mencionan el autismo, las alergias, la esclerosis múltiple y el Alzheimer, que por desgracia se han ido extendiendo por el «boca a boca» y a través de personajes mediáticos con conocimientos científicos escasos o sesgados.</h4>\r\n                                                <h4 style=\"text-align: justify;\">Los grupos antivacunas tienden a infravalorar de forma excesiva las complicaciones de las enfermedades infecciosas, mientras que magnifican los efectos secundarios de las vacunas y ofrecen una visión muy sesgada de la realidad, que resulta alarmista y genera desconfianza gratuita entre la población.</h4>\r\n                                                <br>\r\n                                                <h4 style=\"text-align: justify;\">Fuente: Adaptado de López Heras, D. (2015) ¿Vacunarse o no vacunarse? Seguridad, riesgos y beneficios de las vacunas. A propósito del caso de difteria en Cataluña. En http://www.drlopezheras.com/2015/06/vacunarse-o-novacunarse-seguridad-vacunas.html</h4>\r\n                                               ', '00:04:00', 4),
(2, '<h4 style=\"text-align: center;\"><strong>Texto A</strong></h4>\r\n                                            <h4 style=\"text-align: justify;\">Algunos autores consideran que la modernización de las sociedades lleva a un aumento de la disponibilidad de alimentos, lo cual va asociado a una disminución de las diferencias sociales en la dieta. Esto daría lugar a un aumento en la diversidad de modelos alimentarios que son resultado de la conjunción de criterios de elección individuales diversos, no adscritos exclusivamente a las clases sociales. Así, quienes defienden la desestructuración alimentaria como reflejo del proceso de individualización característico de la modernidad apuestan por una diversidad de pautas alimentarias o modelos plurales en materia de alimentación. En definitiva, la tesis de la modernidad alimentaria se sustenta, por un lado, en la desestructuración de los comportamientos alimentarios y, por otro lado, en el declive de las clases sociales como explicación de las pautas alimentarias de la modernidad.</h4>\r\n                                            <h4 style=\"text-align: justify;\">Poulain (2002) ha realizado un interesante análisis del cambio alimentario considerando que se trata de un proceso de eliminación o privación de algo. Si nos atenemos al prefijo des- que define las tendencias que este autor describe, la modernidad alimentaria se definiría por la desestructuración, la desocialización, la desinstitucionalización, la desimplantación horaria y la desritualización. Los cambios son detectados en la investigación empírica por tres tendencias: la simplificación de la estructura de la comida, el aumento de la ingesta fuera del hogar y el aumento del número de ingestas.</h4>\r\n                                            <br>\r\n                                            <h4 style=\"text-align: center;\"><strong>Texto B</strong></h4>\r\n                                            <h4 style=\"text-align: justify;\">Existen autores que apuestan por una explicación de la diferencia en el consumo de alimentos basados en la diferenciación social. Ellos consideran que los patrones de consumo alimentario se encuentran en relación con el origen social de los consumidores.</h4>\r\n                                            <h4 style=\"text-align: justify;\">Esta orientación, preocupada por el análisis de los desequilibrios sociales, las desigualdades y las relaciones de poder y subordinación, está más próxima a la idea de modelos de consumo de clase, donde pueden observar pautas de reproducción social de la desigualdad.</h4>\r\n                                            <h4 style=\"text-align: justify;\">Warde (1997), en sus estudios llevados a cabo en Gran Bretaña, confirma que la clase social es la variable explicativa de la heterogeneidad alimentaria, al menos cuando la referencia es el gasto alimentario. Las diferencias se dan sobre todo entre la clase obrera y la clase media. La clase obrera consume más pan, carne, grasas, azúcar... aunque la clase media cuenta con diferencias intraclases: unos grupos se acercan más a la alimentación de la clase obrera en sus preferencias alimentarias, otros tienen gustos diferentes. Por otro lado, Lambert (1987), en Francia, plantea que se observa una tendencia a la imitación por parte de las clases populares del modelo emergente de las clases intelectuales urbanas.</h4>\r\n                                            <br>\r\n                                            <h4 style=\"text-align: justify;\">Fuente: Méndez, C. D. (2005). Los debates actuales en la sociología de la alimentación. Revista Internacional de Sociología, 63 (40), 47-48.</h4>\r\n                                            ', '00:04:00', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `content` varchar(250) NOT NULL,
  `action` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `logs`
--

INSERT INTO `logs` (`id`, `content`, `action`, `date`, `user_id`) VALUES
(1, 'contenido', 'eliminar', '2024-07-03', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '0001_01_01_000000_create_users_table', 1),
(2, '0001_01_01_000001_create_cache_table', 1),
(3, '0001_01_01_000002_create_jobs_table', 1),
(4, '2024_08_05_215733_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name_permissions` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name_permissions`) VALUES
(1, 'crear'),
(2, 'editar'),
(3, 'eliminar'),
(4, 'generar reporte'),
(5, 'bloquear'),
(6, 'desbloquear'),
(7, 'visualizar'),
(8, 'no visualizar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions_has_role`
--

CREATE TABLE `permissions_has_role` (
  `permissions_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `permissions_has_role`
--

INSERT INTO `permissions_has_role` (`permissions_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question_text` varchar(255) NOT NULL,
  `section_id` int(11) NOT NULL,
  `type_question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `questions`
--

INSERT INTO `questions` (`id`, `question_text`, `section_id`, `type_question_id`) VALUES
(1, 'Normalmente me divierto y disfruto de la vida', 1, 1),
(2, 'Confío en la gente que conozco', 1, 1),
(3, 'No soy minucioso con los detalles pequeños', 1, 1),
(4, 'Me siento calmada/o', 2, 1),
(5, 'Me siento segura/o', 2, 1),
(6, 'Estoy tensa/o', 2, 1),
(7, 'Estoy contrariada/o', 2, 1),
(8, 'Estoy a gusto', 2, 1),
(9, 'Me siento alterada/o', 2, 1),
(10, 'Estoy preocupada/o actualmente por algún posible contratiempo', 2, 1),
(13, 'Me siento cómoda/o', 2, 1),
(14, 'Me siento con confianza de mí misma/o', 2, 1),
(15, 'Me siento nerviosa/o', 2, 1),
(23, 'Me siento bien', 2, 1),
(24, 'No puedo decidir qué tipo de persona quiero ser', 1, 1),
(25, 'Muestro mis sentimientos a todo el mundo', 1, 1),
(26, 'test question', 1, 1),
(27, 'Trato de analizar las causas del problema para poder hacerles frente', 3, 1),
(28, ' Trato de tener mucha resignación con lo que me sucedió', 3, 1),
(29, 'Procuro relajarme o tranquilizarme a mi manera', 3, 1),
(30, 'Trato de comportarme como si nada hubiera pasado', 3, 1),
(31, '\"Me siento bien\"', 6, 1),
(32, '\"Me canso rápidamente\"', 6, 1),
(33, '\"Siento ganas de llorar\"', 6, 1),
(34, ' \"Quisiera ser tan feliz como otros parecen serlo\"', 6, 1),
(35, '\"Cuando utilizo mi sentido común y mi intuición al momento de tomar decisiones importantes, usualmente éstas resultan correctas en relación a los objetivos planteados.\",', 7, 1),
(36, '\"Al momento de tomar decisiones, tengo una gran confianza en informes o reglamentos, hechos reales y demás informaciones concretas.\"', 7, 1),
(37, '\"Generalmente confío en hechos y cifras más que en mi intuición cuando tengo que tomar una decisión importante.\"', 7, 1),
(38, '\"Siento que tengo una vasta reserva de información útil a la que puedo recurrir para tomar decisiones adecuadas. \"', 7, 1),
(39, 'El texto B constituye, fundamentalmente:', 5, 1),
(40, 'De lo afirmado por el texto B, se puede considerar que los grupos antivacunas', 5, 1),
(41, 'De la argumentación desarrollada en ambos textos, es válido deducir que, si la gente dejara de vacunarse,', 5, 1),
(42, 'La controversia planteada en ambos textos gira en torno a', 10, 1),
(43, 'La frase UNA TENDENCIA A LA IMITACIÓN se debe comprender como:', 10, 1),
(44, 'Las estrategias empleadas en ambos textos son similares, ya que los mismos:', 10, 1),
(45, '¿Cuál es tu nivel de satisfacción con la plataforma?', 8, 1),
(46, '¿Cual es tu impresión de la plataforma respecto a los siguientes aspectos?', 8, 1),
(47, '2.1 Diseño', 8, 1),
(48, '2.2 Facilidad de uso', 8, 1),
(49, '2.3 Tipo de preguntas', 8, 1),
(50, '¿Qué sugerencias y/o comentarios tienes para la mejora de la plataforma?', 8, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `question_responses`
--

CREATE TABLE `question_responses` (
  `id` int(11) NOT NULL,
  `questions_id` int(11) NOT NULL,
  `questions_section_id` int(11) NOT NULL,
  `questions_type_question_id` int(11) NOT NULL,
  `register_id` int(11) NOT NULL,
  `register_exams_id` int(11) NOT NULL,
  `register_gender_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `question_responses`
--

INSERT INTO `question_responses` (`id`, `questions_id`, `questions_section_id`, `questions_type_question_id`, `register_id`, `register_exams_id`, `register_gender_id`) VALUES
(1, 1, 1, 1, 2, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recovery_request`
--

CREATE TABLE `recovery_request` (
  `id` int(11) NOT NULL,
  `request_date` date NOT NULL,
  `validated` tinyint(4) NOT NULL,
  `register_id` int(11) NOT NULL,
  `register_exams_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `lastname` varchar(350) NOT NULL,
  `name` varchar(250) NOT NULL,
  `mail` varchar(350) NOT NULL,
  `birthday` date NOT NULL,
  `graduate` tinyint(4) NOT NULL,
  `date_register` date NOT NULL DEFAULT current_timestamp(),
  `estado_acceso` tinyint(4) NOT NULL DEFAULT 0,
  `exams_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `matricula` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `register`
--

INSERT INTO `register` (`id`, `lastname`, `name`, `mail`, `birthday`, `graduate`, `date_register`, `estado_acceso`, `exams_id`, `gender_id`, `matricula`) VALUES
(1, 'Villavicencio Rodriguez ', 'Maria de los Angeles Veronica', 'Villavicencio@gmail.com', '2000-07-01', 1, '2024-07-04', 1, 1, 1, 0),
(2, 'Caballero Sierra', 'Raymundo Benjamìn', 'Caballero@gmail.com', '1990-07-01', 0, '2024-07-01', 1, 2, 0, 0),
(10, 'Montalvo de Jesús', 'Osvaldo', 'Osvaldo@gmail.com', '1999-01-01', 1, '2024-08-21', 0, 0, 0, 201798745),
(11, 'hola', 'hola', 'hola@hola.com', '2000-02-02', 0, '2024-08-22', 0, 0, 0, NULL),
(27, 'Montalvo de Jesús', 'Maria de los Angeles', 'alexrpnarelle@gmail.com', '2000-01-01', 0, '2024-08-27', 0, 1, 0, NULL),
(28, 'Caballero Sierra', 'Maria de los Angeles', 'alexrpnarelle@gmail.com', '2000-02-02', 0, '2024-08-30', 0, 2, 0, NULL),
(29, 'Montalvo de Jesús', 'Raymundo Benjamin', 'erika.perezrivera.icgde@viep.com.mx', '2000-02-02', 1, '2024-09-02', 0, 1, 0, 201785412);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `role_name`) VALUES
(1, 'admin'),
(2, 'especialista'),
(3, 'usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_user`
--

CREATE TABLE `role_has_user` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `role_has_user`
--

INSERT INTO `role_has_user` (`role_id`, `user_id`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `name_section` varchar(255) NOT NULL,
  `description_section` varchar(800) NOT NULL,
  `exams_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `section`
--

INSERT INTO `section` (`id`, `name_section`, `description_section`, `exams_id`) VALUES
(1, 'Test01', 'Test01. El propósito de este cuestionario es conocer qué tipo de persona has sido en los últimos 5 años.\r\nPor favor, no omitas ningún ítem. Si no estás seguro de una respuesta, señala la respuesta [VERDADERO o FALSO] que te parezca más correcta.\r\nNo hay límite de tiempo, pero no lo pierdas pensando cuál es la respuesta correcta a un ítem determinado.', 1),
(2, 'Test02', 'Test02. Algunas expresiones que la gente usa para describirse aparecen abajo. Lea cada frase y marque el número que indique cómo se siente <strong><em>ahora mismo</em>, o sea, <em>en este momento</em></strong>. No hay contestaciones buenas o malas. No emplee mucho tiempo para cada frase, pero trate de dar la respuesta que mejor describa sus sentimientos ahora.', 1),
(3, 'Test03', 'Test03.\r\nA continuación, se plantean diferentes formas que emplean las personas para afrontar los problemas o situaciones estresantes que se les presentan en la vida. Las formas de afrontamiento aquí descritas no son ni buenas ni malas, ni mejores o peores. Simplemente ciertas personas utilizan unas formas más que otras, dependiendo de la situación problema.\r\n\r\nTrate de recordar las diferentes situaciones o problemas más estresantes vividos durante los últimos años, y seleccione aquella respuesta que mejor indique que tan habitual ha sido esta forma de comportamiento ante las situaciones estresantes.', 1),
(4, 'text04_L1', 'Lectura 1. Por favor lea detenidamente el siguiente texto, tienes cuatro minutos como tiempo máximo para hacerlo, una vez concluido el tiempo o al presionar el botón \"Siguiente\" no podrá regresar a la lectura.', 1),
(5, 'test04_L1', 'Por favor responda las siguientes preguntas', 1),
(6, 'Test05', 'Test05. Algunas expresiones que la gente usa para describirse aparecen abajo. Lea cada frase y seleccione la respuesta que indique cómo se siente \r\n                                    <strong><em>generalmente</em></strong>. No hay contestaciones buenas o malas. No emplee mucho tiempo para cada frase, pero trate de dar la respuesta que mejor describa \r\n                                    <strong><em>cómo se siente generalmente</em></strong>.', 1),
(7, 'test06', 'Test06. Algunas expresiones que la gente usa para describirse aparecen abajo. Lea cada frase y seleccione la respuesta que indique cómo se siente <strong><em>generalmente</em></strong>. \r\nNo hay contestaciones buenas o malas. No emplee mucho tiempo para cada frase, pero trate de dar la respuesta que mejor describa <strong><em>cómo se siente generalmente</em></strong>.\r\n                                  ', 1),
(8, 'encuesta', 'Brindarte una buena atención es nuestro principal objetivo, te agradecemos nos apoyes a contestar la siguiente encuesta.', 1),
(9, 'text04_L2', 'Lectura 2. Por favor lea detenidamente el siguiente texto, tienes cuatro minutos como tiempo máximo para hacerlo, una vez concluido el tiempo o al presionar el botón \"Siguiente\" no podrá regresar a la lectura.', 1),
(10, 'test04_L2', 'Por favor responda las siguientes preguntas', 1),
(15, 'Seccion de prueba', 'descripcion de prueba', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `user_agent` text DEFAULT NULL,
  `payload` longtext NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('MxN9gry8yJNfOUBSEhdIvNBPbbnieM3SlrbLdFud', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiMjRnYTdqSEt3ZlMyajRUSzBUaHVFZ3cwV2xObjhxTzdTWENmb1BxMCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHA6Ly9sb2NhbGhvc3QvZGIvcHVibGljL3VzZXIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1725303146);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_question`
--

CREATE TABLE `type_question` (
  `id` int(11) NOT NULL,
  `type_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `type_question`
--

INSERT INTO `type_question` (`id`, `type_name`) VALUES
(1, 'multiple'),
(2, 'abierto'),
(3, 'radio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(16) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `mail` varchar(350) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `active`, `mail`) VALUES
(1, 'usernumber1', 'ecddd72b4f47858247099553784a9879', 1, 'usernumber1@gmail.com'),
(2, 'usernumber2', 'df6d4fdff1f63ca13f745e80bc7c9a9d', 0, 'usernumber2@gmail.com'),
(3, 'usernumber3', 'e399274eff72a318a5a14c1a18a488c0', 1, 'usernumber3@gmail.com'),
(4, 'narelle.rodrigue', 'ecadb762ad1294e9e79d3c20b16fea48', 1, 'narelle.rodriguez@gmail.com'),
(5, 'usernumber5', '5de5d5429217c269542e622867b4970a', 1, 'usernumber5@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`,`questions_id`),
  ADD KEY `fk_answers_questions1_idx` (`questions_id`);

--
-- Indices de la tabla `answers_responses`
--
ALTER TABLE `answers_responses`
  ADD PRIMARY KEY (`id`,`answers_id`,`answers_questions_id`,`register_id`,`register_exams_id`,`register_gender_id`),
  ADD KEY `fk_answers_responses_answers1_idx` (`answers_id`,`answers_questions_id`),
  ADD KEY `fk_answers_responses_register1_idx` (`register_id`,`register_exams_id`,`register_gender_id`);

--
-- Indices de la tabla `cache`
--
ALTER TABLE `cache`
  ADD PRIMARY KEY (`key`);

--
-- Indices de la tabla `cache_locks`
--
ALTER TABLE `cache_locks`
  ADD PRIMARY KEY (`key`);

--
-- Indices de la tabla `encuesta`
--
ALTER TABLE `encuesta`
  ADD PRIMARY KEY (`id`,`register_id`,`register_user_id`,`register_exams_id`,`register_gender_id`,`section_id`,`section_exams_id`),
  ADD KEY `fk_encuesta_register1_idx` (`register_id`,`register_user_id`,`register_exams_id`,`register_gender_id`),
  ADD KEY `fk_encuesta_section1_idx` (`section_id`,`section_exams_id`),
  ADD KEY `fk_encuesta_register1` (`register_id`,`register_exams_id`,`register_gender_id`);

--
-- Indices de la tabla `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `filtrer_logs`
--
ALTER TABLE `filtrer_logs`
  ADD PRIMARY KEY (`id`,`logs_id`,`logs_user_id`),
  ADD KEY `fk_filtrer_logs_logs1_idx` (`logs_id`,`logs_user_id`);

--
-- Indices de la tabla `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indices de la tabla `job_batches`
--
ALTER TABLE `job_batches`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lecturas`
--
ALTER TABLE `lecturas`
  ADD PRIMARY KEY (`id`,`section_id`),
  ADD KEY `fk_lecturas_cuestionario1_idx` (`section_id`);

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_logs_user1_idx` (`user_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permissions_has_role`
--
ALTER TABLE `permissions_has_role`
  ADD PRIMARY KEY (`permissions_id`,`role_id`),
  ADD KEY `fk_permissions_has_role_role1_idx` (`role_id`),
  ADD KEY `fk_permissions_has_role_permissions1_idx` (`permissions_id`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`,`section_id`,`type_question_id`),
  ADD KEY `fk_questions_section1_idx` (`section_id`),
  ADD KEY `fk_questions_type_question1_idx` (`type_question_id`);

--
-- Indices de la tabla `question_responses`
--
ALTER TABLE `question_responses`
  ADD PRIMARY KEY (`id`,`questions_id`,`questions_section_id`,`questions_type_question_id`,`register_id`,`register_exams_id`,`register_gender_id`),
  ADD KEY `fk_question_responses_questions1_idx` (`questions_id`,`questions_section_id`,`questions_type_question_id`),
  ADD KEY `fk_question_responses_register1_idx` (`register_id`,`register_exams_id`,`register_gender_id`);

--
-- Indices de la tabla `recovery_request`
--
ALTER TABLE `recovery_request`
  ADD PRIMARY KEY (`id`,`register_id`,`register_exams_id`),
  ADD KEY `fk_recovery_request_register1_idx` (`register_id`,`register_exams_id`);

--
-- Indices de la tabla `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`,`exams_id`,`gender_id`),
  ADD KEY `fk_register_exams1_idx` (`exams_id`),
  ADD KEY `fk_register_gender1_idx` (`gender_id`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_has_user`
--
ALTER TABLE `role_has_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `fk_role_has_user_user1_idx` (`user_id`),
  ADD KEY `fk_role_has_user_role1_idx` (`role_id`);

--
-- Indices de la tabla `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`,`exams_id`),
  ADD KEY `fk_section_exams1_idx` (`exams_id`);

--
-- Indices de la tabla `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indices de la tabla `type_question`
--
ALTER TABLE `type_question`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT de la tabla `answers_responses`
--
ALTER TABLE `answers_responses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `encuesta`
--
ALTER TABLE `encuesta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lecturas`
--
ALTER TABLE `lecturas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `question_responses`
--
ALTER TABLE `question_responses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `recovery_request`
--
ALTER TABLE `recovery_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `recovery_request`
--
ALTER TABLE `recovery_request`
  ADD CONSTRAINT `fk_recovery_request_register1` FOREIGN KEY (`register_id`,`register_exams_id`) REFERENCES `register` (`id`, `exams_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
